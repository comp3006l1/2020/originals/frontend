import React from "react";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import NetworkService from "./networkservice";

import RouterLinks from "./Router";

//redux
import configureStore from "./Store/ConfigureStore";

//store
const store = configureStore();

//Here is the guy where I set up the interceptors!
NetworkService.setupInterceptors(store);

function App() {
  return (
    <Provider store={store}>
      <RouterLinks />
      <ToastContainer
        position="top-center"
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnVisibilityChange
        draggable
        pauseOnHover
      />
    </Provider>
  );
}

export default App;
