import React, { useState, useEffect, Suspense, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import { fetchClockAction } from "../../Redux/Clock/ClockAction";
import {
  fetchWorkersAction,
  getMapAction,
} from "../../Redux/Simulator/SimulatorAction";

//styles
import "./home.scss";

//local components
import Orders from "./Components/Orders";
import Graph from "./Components/Graph";
import Worker from "./Components/Worker";
import Configuration from "./Components/Configuration";
import WorkerAction from "./Components/WorkerAction";
import WorkerOrder from "./Components/WorkerOrder";

//global components
import { PrimaryButton, OutlineButton } from "../../Components/Buttons";
import { EmptyLoader, EmptyReportLoader } from "../../Components/Loader";

const getEdges = (edgesnew) => {
  let edgesList = [];

  edgesnew.map((edge, index) => {
    let vertex1 = edge.split("->")[0];
    let vertex2 = edge.split("->")[1];

    edgesList = [
      ...edgesList,
      {
        source: vertex1.trim(),
        target: vertex2.trim(),
        type: "emptyEdge",
      },
    ];
  });

  return edgesList;
};

const getNewNodes = (vertices) => {
  const nodeList = vertices.map((vertice, index) => {
    let aisle = vertice.split("a")[1].split(".")[0];
    let section = vertice.split("a")[1].split(".")[1];

    // aisle = Number(aisle) === 0 ? 0.2 : aisle;
    // section = Number(section) === 0 ? 0.2 : section;

    console.log("vertice--", index, vertice, section, aisle);

    return {
      id: vertice,
      title: vertice,
      x: (10000 / vertices.length) * Number(section),
      y: 450 * Number(aisle),
      type: "empty",
    };
  });

  return nodeList;
};

const getWorkerNode = (nodes, location) => {
  return nodes.find((x) => x.id === location);
};

export const Home = (props) => {
  const dispatch = useDispatch();
  const clock = useSelector((state) => state.clock.clock);
  const map = useSelector((state) => state.simulator.map);
  const selectedWorker = useSelector((state) => state.simulator.selectedWorker);
  const selectedWorkerDetail = useSelector(
    (state) => state.simulator.selectedWorkerDetail
  );

  const [toggleOrders, setToggleOrders] = useState(false);
  const [toggleConfiguration, setToggleConfiguration] = useState(false);

  const nodes = useMemo(
    () => (map.data ? getNewNodes(map.data.vertices) : []),
    [map.data]
  );

  const edges = useMemo(() => (map.data ? getEdges(map.data.edges) : []), [
    map.data,
  ]);

  const selected = useMemo(
    () =>
      selectedWorkerDetail.data
        ? getWorkerNode(nodes, selectedWorkerDetail.data.location)
        : [],
    [nodes, selectedWorkerDetail.data]
  );

  useEffect(() => {
    dispatch(fetchClockAction());
    dispatch(fetchWorkersAction());
    dispatch(getMapAction());
  }, []);

  return (
    <>
      <div className="home_main">
        <Grid container spacing={3} className="mainConatiner">
          <Grid item xs={12} className="mainHeader">
            <h1>
              Distribution Center <span>by group - originals</span>
            </h1>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={3}>
              <Grid item xs={9}>
                <Grid item xs={12} className="settingsContainer">
                  <div className="btnContainer">
                    <PrimaryButton onClick={() => setToggleOrders(true)}>
                      View Orders
                    </PrimaryButton>
                    <OutlineButton onClick={() => setToggleConfiguration(true)}>
                      Configure
                    </OutlineButton>
                  </div>
                  <div>
                    <Worker />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className="graphContainer">
                    {map.data ? (
                      <Graph nodes={nodes} edges={edges} selected={selected} />
                    ) : (
                      <div className="graphEmpty">
                        <EmptyReportLoader width={200} />
                        <h1>Please set configuration to start the simulator</h1>
                        <PrimaryButton
                          onClick={() => setToggleConfiguration(true)}
                        >
                          Configure
                        </PrimaryButton>
                      </div>
                    )}
                    {/* <Graph nodes={nodes} edges={edges} /> */}
                  </div>
                </Grid>
              </Grid>
              <Grid item xs={3}>
                <WorkerAction />
                <WorkerOrder />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
      <Orders open={toggleOrders} handleClose={() => setToggleOrders(false)} />
      <Configuration
        open={toggleConfiguration}
        handleClose={() => setToggleConfiguration(false)}
      />
    </>
  );
};

export default Home;
