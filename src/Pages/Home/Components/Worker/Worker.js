import React, { useState, useEffect, useMemo, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";

//redux
import {
  fetchWorkersAction,
  selectWorkersAction,
  getWorkerByIDAction,
} from "../../../../Redux/Simulator/SimulatorAction";

//styles
import "./worker.scss";

//global components
import { SelectBox } from "../../../../Components/FormInput";

const workerData = [
  {
    key: "kasun",
    value: "kasun",
  },
  {
    key: "kalana",
    value: "kalana",
  },
  {
    key: "sachin",
    value: "sachin",
  },
];

const generateWorkerList = (workers) => {
  return workers.map((data) => {
    return {
      key: data,
      value: data,
    };
  });
};

export const Worker = (props) => {
  const dispatch = useDispatch();
  const workers = useSelector((state) => state.simulator.workers);

  const [selectedWorker, setSelectedWorker] = useState("");

  const workerList = useMemo(
    () => (workers.data ? generateWorkerList(workers.data) : []),
    workers.data
  );

  useEffect(() => {
    dispatch(fetchWorkersAction());
  }, []);

  useEffect(() => {
    if (selectedWorker) {
      dispatch(selectWorkersAction(selectedWorker));
      // dispatch(getWorkerByIDAction(selectedWorker));
    }
  }, [selectedWorker]);

  return (
    <>
      <div className="worker">
        <SelectBox
          data={workerList}
          labelValue="Select worker to visualize"
          labelClass="formLabel"
          formClass="formGroup"
          className="selectBox"
          value={selectedWorker}
          onChange={(e) => setSelectedWorker(e.target.value)}
          // inputError={errors.selectedFilesTwoError}
        />
      </div>
    </>
  );
};

export default Worker;
