import React, { useState, useEffect, Suspense, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import { fetchOrderAction } from "../../../../Redux/Order/OrderAction";

//styles
import "./orders.scss";

//components
import { ModalComponent } from "../../../../Components/Modal";

export const convertDate = (
  dateVal,
  option = {
    weekday: "short",
  }
) => {
  var date = new Date(dateVal);

  const dateString = `${new Intl.DateTimeFormat("en-US", option).format(
    date
  )} ${date.getMonth() + 1} ${date.getDate()} ${date.getFullYear()}`;
  return dateString;
};

export const Orders = ({ open = false, handleClose = () => {} }) => {
  const dispatch = useDispatch();
  const order = useSelector((state) => state.order.order);

  const [time, setTime] = useState(Date.now());

  useEffect(() => {
    dispatch(fetchOrderAction());
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(Date.now());
      dispatch(fetchOrderAction());
    }, 1000);
    if (!open) {
      clearInterval(interval);
    }
    return () => {
      clearInterval(interval);
    };
  }, [open]);

  return (
    <>
      <ModalComponent open={open} handleClose={handleClose} showCloseBtn={true}>
        <div className="modalContainer">
          <h1 className="headerModal">Orders</h1>
          <Grid container spacing={3}>
            {order.data &&
              order.data.orders.map((data) => (
                <Grid item xs={12}>
                  <Paper className="orderContainer">
                    <div className="orderContainerHeader">
                      <div>
                        <h1>Order ID : {data.id}</h1>
                      </div>
                      <div className="statusContainer">
                        <p>{data.status}</p>
                      </div>
                    </div>

                    <div className="cardContainer">
                      {data.items.map((item) => (
                        <Card className="card">
                          <CardContent>
                            <h1>{item.name}</h1>
                            <p className="subHeader">{item.supplier}</p>
                            <div className="itemSubDetails">
                              <p>location - {item.location}</p>
                              <p>weight - {item.weight}</p>
                            </div>
                          </CardContent>
                        </Card>
                      ))}
                    </div>
                    <div className="orderContainerFooter">
                      <p>{convertDate(data.createdAt)}</p>
                    </div>
                  </Paper>
                </Grid>
              ))}
          </Grid>
        </div>
      </ModalComponent>
    </>
  );
};

export default Orders;
