import React, { useState, useEffect, useMemo, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import { getWorkerByIDAction } from "../../../../Redux/Simulator/SimulatorAction";

//styles
import "./workerAction.scss";

//global components
import { SelectBox } from "../../../../Components/FormInput";
import { EmptyLoader } from "../../../../Components/Loader";

const actionList = [
  {
    type: "MOVE",
    arguments: ["a1.2"],
    success: true,
    step: 478,
  },
  {
    type: "MOVE",
    arguments: ["a1.2"],
    success: null,
    step: 478,
  },
  {
    type: "DROP",
    arguments: ["a1.2"],
    success: true,
    step: 478,
  },
];

export const Worker = (props) => {
  const dispatch = useDispatch();
  const selectedWorker = useSelector((state) => state.simulator.selectedWorker);
  const selectedWorkerDetail = useSelector(
    (state) => state.simulator.selectedWorkerDetail
  );

  const [time, setTime] = useState(Date.now());

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(Date.now());
      if (selectedWorker) dispatch(getWorkerByIDAction(selectedWorker));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [time]);

  useEffect(() => {
    if (selectedWorker) dispatch(getWorkerByIDAction(selectedWorker));
  }, [selectedWorker]);

  return (
    <>
      <div className="workerAction">
        <Grid container spacing={3} className="workerContainer">
          <Grid item xs={12} className="workerContainerheader">
            <h1>Action List</h1>
          </Grid>
          {selectedWorkerDetail.data ? (
            selectedWorkerDetail.data.actions.map((data) => (
              <Grid item xs={12} className="workerCardContainer">
                <Paper className="workerCard">
                  <div className="infoContainer">
                    <h1>{data.type}</h1>
                    <p>{data.arguments}</p>
                  </div>
                  <div className="cardFooter">
                    <p
                      className={`${data.success ? "completed" : "processing"}`}
                    >
                      {data.success ? "COMPLETED" : "PROCESSING"}
                    </p>
                  </div>
                </Paper>
              </Grid>
            ))
          ) : (
            <div className="graphEmpty">
              <EmptyLoader width={100} height={100} />
              <h1>Please select the user to view their actions</h1>
            </div>
          )}
        </Grid>
      </div>
    </>
  );
};

export default Worker;
