import React, { useState, useEffect, Suspense, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import { setConfigurationAction } from "../../../../../Redux/Simulator/SimulatorAction";

//components
import { TextInput } from "../../../../../Components/FormInput";
import { PrimaryButton } from "../../../../../Components/Buttons";

//utils
import { validateStepOne } from "./Utils/ValidateForm";

export const StepOne = ({ goToNext = () => {} }) => {
  const dispatch = useDispatch();
  const order = useSelector((state) => state.order.order);

  const [aisle, setAisle] = useState("");
  const [section, setSection] = useState("");
  const [shelves, setShelves] = useState("");
  const [packingAreas, setPackingAreas] = useState([]);

  const [packingAreasTemp, setPackingAreasTemp] = useState("");

  const [errors, setErrors] = useState({
    aisleError: "",
    sectionerror: "",
    shelvesError: "",
    packingAreasError: "",
  });

  const addPackingArea = (val) => {
    setPackingAreas([...packingAreas, val]);
  };

  const nextPage = (val) => {
    console.log("working");
    const [flag, validationErrors] = validateStepOne({
      aisle,
      section,
      shelves,
      packingAreas,
    });

    if (flag) {
      dispatch(
        setConfigurationAction({
          aisles: Number(aisle),
          sections: Number(section),
          shelves: Number(shelves),
          packagingAreas: packingAreas,
        })
      );
      goToNext(2);
    } else {
      setErrors(validationErrors);
    }
  };

  return (
    <>
      <form>
        <h1>Configure Map</h1>
        <TextInput
          labelClass="formLabel"
          labelValue="Aisles"
          formClass="formGroup"
          className="selectBox"
          value={aisle}
          onChange={(e) => setAisle(e.target.value)}
          inputError={errors.aisleError}
          type="number"
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Sections"
          formClass="formGroup"
          className="selectBox"
          value={section}
          onChange={(e) => setSection(e.target.value)}
          inputError={errors.sectionerror}
          type="number"
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Shelves"
          formClass="formGroup"
          className="selectBox"
          value={shelves}
          onChange={(e) => setShelves(e.target.value)}
          inputError={errors.shelvesError}
          type="number"
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Packing Areas"
          formClass="formGroup"
          className="selectBox"
          value={packingAreasTemp}
          onChange={(e) => setPackingAreasTemp(e.target.value)}
          inputError={errors.packingAreasError}
          InputProps={{
            endAdornment: (
              <PrimaryButton
                className="endAdormantBtn"
                onClick={() => addPackingArea(packingAreasTemp)}
              >
                Add
              </PrimaryButton>
            ),
          }}
        />
        <div className="packingAreaContainer">
          <p>Packing Areas : </p>
          {packingAreas.length > 0 ? (
            packingAreas.map((data) => <div className="card">{data}</div>)
          ) : (
            <p>Empty</p>
          )}
        </div>

        <PrimaryButton className="sendBtn" onClick={() => nextPage()}>
          Next
        </PrimaryButton>
      </form>
    </>
  );
};

export default StepOne;
