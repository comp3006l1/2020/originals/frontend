import React, { useState, useEffect, Suspense, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import { setConfigurationAction } from "../../../../../Redux/Simulator/SimulatorAction";

//components
import { TextInput } from "../../../../../Components/FormInput";
import {
  PrimaryButton,
  OutlineButton,
} from "../../../../../Components/Buttons";

//utils
import { validateStepOne } from "./Utils/ValidateForm";

export const StepOne = ({ goToNext = () => {} }) => {
  const dispatch = useDispatch();
  const order = useSelector((state) => state.order.order);

  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [capacity, setCapacity] = useState("");
  const [workers, setWorkers] = useState([]);

  const [errors, setErrors] = useState({
    nameError: "",
    locationError: "",
    capacityError: "",
  });

  const addWorker = (val) => {
    setWorkers([
      ...workers,
      {
        name,
        location,
        capacity,
      },
    ]);
  };

  const nextPage = (val) => {
    // console.log("working");
    // const [flag, validationErrors] = validateStepOne({
    //   workers,
    // });

    // if (flag) {
    if (workers.length > 0) {
      dispatch(setConfigurationAction({ workers }));
      goToNext(3);
    }

    // } else {
    //   setErrors(validationErrors);
    // }
  };

  return (
    <>
      <form>
        <h1>Add Workers</h1>
        <TextInput
          labelClass="formLabel"
          labelValue="Worker Name"
          formClass="formGroup"
          className="selectBox"
          value={name}
          onChange={(e) => setName(e.target.value)}
          inputError={errors.aisleError}
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Location"
          formClass="formGroup"
          className="selectBox"
          value={location}
          onChange={(e) => setLocation(e.target.value)}
          inputError={errors.sectionerror}
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Capacity"
          formClass="formGroup"
          className="selectBox"
          value={capacity}
          onChange={(e) => setCapacity(e.target.value)}
          inputError={errors.shelvesError}
        />

        <OutlineButton className="sendBtn" onClick={() => addWorker()}>
          Add
        </OutlineButton>

        <div className="packingAreaContainer">
          <p>Workers : </p>
          {workers.length > 0 ? (
            workers.map((data) => (
              <div className="card">
                <h1>{data.name}</h1>
                <p>location - {data.location}</p>
                <p>capacity - {data.capacity}</p>
              </div>
            ))
          ) : (
            <p>Empty</p>
          )}
        </div>

        <PrimaryButton className="sendBtn" onClick={() => nextPage()}>
          Next
        </PrimaryButton>
      </form>
    </>
  );
};

export default StepOne;
