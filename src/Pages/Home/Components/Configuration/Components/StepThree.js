import React, { useState, useEffect, Suspense, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import { setConfigurationAction } from "../../../../../Redux/Simulator/SimulatorAction";

//components
import { TextInput } from "../../../../../Components/FormInput";
import {
  PrimaryButton,
  OutlineButton,
} from "../../../../../Components/Buttons";

//utils
import { validateStepOne } from "./Utils/ValidateForm";

export const StepThree = ({ goToNext = () => {} }) => {
  const dispatch = useDispatch();
  const order = useSelector((state) => state.order.order);

  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [supplier, setSupplier] = useState("");
  const [weight, setWeight] = useState("");
  const [location, setLocation] = useState("");

  const [items, setItems] = useState([]);

  const [errors, setErrors] = useState({
    nameError: "",
    locationError: "",
    capacityError: "",
  });

  const addWorker = (val) => {
    setItems([
      ...items,
      {
        id,
        name,
        supplier,
        weight,
        location,
      },
    ]);
  };

  const nextPage = (val) => {
    // console.log("working");
    // const [flag, validationErrors] = validateStepOne({
    //   workers,
    // });

    // if (flag) {
    if (items.length > 0) {
      dispatch(setConfigurationAction({ items }));
      goToNext(3);
    }

    // } else {
    //   setErrors(validationErrors);
    // }
  };

  return (
    <>
      <form>
        <h1>Add Items</h1>
        <TextInput
          labelClass="formLabel"
          labelValue="Item ID"
          formClass="formGroup"
          className="selectBox"
          value={id}
          onChange={(e) => setId(e.target.value)}
          //   inputError={errors.shelvesError}
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Item Name"
          formClass="formGroup"
          className="selectBox"
          value={name}
          onChange={(e) => setName(e.target.value)}
          //   inputError={errors.aisleError}
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Item Supplier"
          formClass="formGroup"
          className="selectBox"
          value={supplier}
          onChange={(e) => setSupplier(e.target.value)}
          //   inputError={errors.sectionerror}
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Weight"
          formClass="formGroup"
          className="selectBox"
          value={weight}
          onChange={(e) => setWeight(e.target.value)}
          //   inputError={errors.shelvesError}
        />
        <TextInput
          labelClass="formLabel"
          labelValue="Weight"
          formClass="formGroup"
          className="selectBox"
          value={location}
          onChange={(e) => setLocation(e.target.value)}
          //   inputError={errors.shelvesError}
        />

        <OutlineButton className="sendBtn" onClick={() => addWorker()}>
          Add Item
        </OutlineButton>

        <div className="packingAreaContainer">
          <p>Workers : </p>
          {items.length > 0 ? (
            items.map((data) => (
              <div className="card">
                <h1>{data.name}</h1>
                <h5>{data.id}</h5>
                <p>supplier - {data.supplier}</p>
                <p>weight - {data.weight}</p>
              </div>
            ))
          ) : (
            <p>Empty</p>
          )}
        </div>

        <PrimaryButton className="sendBtn" onClick={() => nextPage()}>
          Submit
        </PrimaryButton>
      </form>
    </>
  );
};

export default StepThree;
