import { validateSingleInput } from "../../../../../../Utils/ValidateSchema";

/**
 * Validate input data
 *
 * @param {*} state input data object
 * @returns flag and errors object
 */
export const validateStepOne = (state) => {
  let flag = true; // flag true = no errors, false = have errors

  let errObj = {
    aisleError: "",
    sectionerror: "",
    shelvesError: "",
    packingAreasError: "",
  }; //store errors returned

  let aisleError = !state.aisle ? "Please enter number of ailses" : "";
  let sectionerror = !state.section ? "Please enter number of sections" : "";
  let shelvesError = !state.shelves ? "Please enter number of sheleves" : "";
  let packingAreasError =
    state.packingAreas.length === 0 ? "Please enter packing areas" : "";

  if (aisleError) {
    errObj = {
      ...errObj,
      aisleError,
    };
    flag = false;
  }

  if (sectionerror) {
    errObj = {
      ...errObj,
      sectionerror,
    };
    flag = false;
  }

  if (shelvesError) {
    errObj = {
      ...errObj,
      shelvesError,
    };
    flag = false;
  }

  if (packingAreasError) {
    errObj = {
      ...errObj,
      packingAreasError,
    };
    flag = false;
  }

  return [flag, errObj];
};
