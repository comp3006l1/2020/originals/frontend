import React, { useState, useEffect, Suspense, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import {
  submitConfigurationAction,
  getMapAction,
  fetchWorkersAction,
} from "../../../../Redux/Simulator/SimulatorAction";

//styles
import "./configuration.scss";

//components
import { ModalComponent } from "../../../../Components/Modal";
import { TextInput } from "../../../../Components/FormInput";
import { PrimaryButton } from "../../../../Components/Buttons";

//local components
import StepOne from "./Components/StepOne";
import StepTwo from "./Components/StepTwo";
import StepThree from "./Components/StepThree";

//notifications
import { successNoty, errorNoty } from "../../../../Utils/Notifications";

export const Configuration = ({ open = false, handleClose = () => {} }) => {
  const dispatch = useDispatch();
  const configuration = useSelector((state) => state.simulator.configuration);
  const config = useSelector((state) => state.simulator.config);

  const [page, setPage] = useState(1);

  // const [time, setTime] = useState(Date.now());

  // useEffect(() => {
  //   dispatch(fetchOrderAction());
  // }, []);

  useEffect(() => {
    if (config.data) {
      handleClose();
      successNoty({
        msg: `Configuration added successfully!`,
      });
      dispatch(getMapAction());
      dispatch(fetchWorkersAction());
    } else if (config.status === 400) {
      errorNoty({
        msg: `Configuration failed!`,
      });
    }
  }, [config.data]);

  useEffect(() => {
    if (configuration.items)
      dispatch(submitConfigurationAction({ ...configuration }));
  }, [configuration]);

  const submitConfig = () => {
    if (configuration.items)
      dispatch(submitConfigurationAction({ ...configuration }));
  };

  return (
    <>
      <ModalComponent open={open} handleClose={handleClose} showCloseBtn={true}>
        <div className="modalContainerConfigure">
          <h1 className="headerModal">Configuration</h1>
          {page === 1 && <StepOne goToNext={(x) => setPage(x)} />}
          {page === 2 && <StepTwo goToNext={(x) => setPage(x)} />}
          {page === 3 && <StepThree goToNext={(x) => submitConfig(x)} />}
        </div>
      </ModalComponent>
    </>
  );
};

export default Configuration;
