import React, { Component } from "react";
import {
  GraphView, // required
  Edge, // optional
  IEdge, // optional
  Node, // optional
  INode, // optional
  LayoutEngineType, // required to change the layoutEngineType, otherwise optional
  BwdlTransformer, // optional, Example JSON transformer
  GraphUtils, // optional, useful utility functions
} from "react-digraph";

import "./graph.scss";

const GraphConfig = {
  NodeTypes: {
    empty: {
      // required to show empty nodes
      typeText: "None",
      shapeId: "#empty", // relates to the type property of a node
      shape: (
        <symbol viewBox="0 0 100 100" id="empty" key="0">
          <circle cx="50" cy="50" r="45"></circle>
        </symbol>
      ),
    },
    custom: {
      // required to show empty nodes
      typeText: "Custom",
      shapeId: "#custom", // relates to the type property of a node
      shape: (
        <symbol viewBox="0 0 50 25" id="custom" key="0">
          <ellipse cx="50" cy="25" rx="50" ry="25"></ellipse>
        </symbol>
      ),
    },
  },
  NodeSubtypes: {},
  EdgeTypes: {
    emptyEdge: {
      // required to show empty edges
      shapeId: "#emptyEdge",
      shape: (
        <symbol viewBox="0 0 50 50" id="emptyEdge" key="0">
          <circle cx="25" cy="25" r="8" fill="currentColor">
            {" "}
          </circle>
        </symbol>
      ),
    },
  },
};

const NODE_KEY = "id"; // Allows D3 to correctly update DOM

let vertices = [
  "a1.0",
  "a1.1",
  "a1.2",
  "a1.3",
  "a1.4",
  "a1.5",
  "a2.0",
  "a2.1",
  "a2.2",
  "a2.3",
  "a2.4",
  "a2.5",
];

let edgesnew = [
  "a1.0 -> a1.1",
  "a1.1 -> a1.2",
  "a1.2 -> a1.3",
  "a1.3 -> a1.4",
  "a1.4 -> a1.5",
  "a1.5 -> a2.5",
  "a2.0 -> a2.1",
  "a2.1 -> a2.2",
  "a2.2 -> a2.3",
  "a2.3 -> a2.4",
  "a2.4 -> a2.5",
  "a2.0 -> a1.0",
];

const nodes = [
  {
    id: 1,
    title: "Node A",
    x: 258.3976135253906,
    y: 331.9783248901367,
    type: "empty",
  },
  {
    id: 2,
    title: "Node B",
    x: 593.9393920898438,
    y: 260.6060791015625,
    type: "empty",
  },
  {
    id: 3,
    title: "Node A",
    x: 258.3976135253906,
    y: 331.9783248901367,
    type: "empty",
  },
  {
    id: 4,
    title: "Node B",
    x: 593.9393920898438,
    y: 260.6060791015625,
    type: "empty",
  },
];

const edges = [
  {
    source: 1,
    target: 2,
    type: "emptyEdge",
  },
  {
    source: 2,
    target: 4,
    type: "emptyEdge",
  },
];

class Graph extends Component {
  constructor(props) {
    super(props);
  }

  // Node 'mouseUp' handler
  onSelectNode = (viewNode, event) => {
    // const { id = "" } = event.target;
    // if (id.includes("text")) {
    //   document.getElementById(event.target.id).click();
    // }

    console.log("selected", viewNode);

    // Deselect events will send Null viewNode
    this.setState({ selected: viewNode });
  };

  /* Define custom graph editing methods here */

  render() {
    const nodes = this.props.nodes;
    const edges = this.props.edges;
    const selected = this.props.selected;

    const NodeTypes = GraphConfig.NodeTypes;
    const NodeSubtypes = GraphConfig.NodeSubtypes;
    const EdgeTypes = GraphConfig.EdgeTypes;

    return (
      <div id="graph" className="graphBody">
        <GraphView
          ref="GraphView"
          nodeKey={NODE_KEY}
          nodes={nodes}
          edges={edges}
          selected={selected}
          nodeTypes={NodeTypes}
          nodeSubtypes={NodeSubtypes}
          edgeTypes={EdgeTypes}
          onSelectNode={this.onSelectNode}
          onCreateNode={this.onCreateNode}
          onUpdateNode={this.onUpdateNode}
          onDeleteNode={this.onDeleteNode}
          onSelectEdge={this.onSelectNode}
          onCreateEdge={this.onCreateEdge}
          onSwapEdge={this.onSwapEdge}
          onDeleteEdge={this.onDeleteEdge}
        />
      </div>
    );
  }
}

export default Graph;
