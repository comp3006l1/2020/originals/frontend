import React, { useState, useEffect, useMemo, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Paper, Grid, Card, CardContent } from "@material-ui/core";

//redux
import { fetchWorkersOrdersAction } from "../../../../Redux/Managment/ManagmentAction";

//styles
import "./workerOrder.scss";

//global components
import { SelectBox } from "../../../../Components/FormInput";
import { EmptyLoader } from "../../../../Components/Loader";

const workerData = [
  {
    key: "kasun",
    value: "kasun",
  },
  {
    key: "kalana",
    value: "kalana",
  },
  {
    key: "sachin",
    value: "sachin",
  },
];

const convertDate = (
  dateVal,
  option = {
    weekday: "short",
  }
) => {
  var date = new Date(dateVal);

  const dateString = `${new Intl.DateTimeFormat("en-US", option).format(
    date
  )} ${date.getMonth() + 1} ${date.getDate()} ${date.getFullYear()}`;
  return dateString;
};
export const WorkerOrder = (props) => {
  const dispatch = useDispatch();
  const orderWorker = useSelector((state) => state.managment.orderWorker);
  const completedOrders = useSelector(
    (state) => state.managment.completedOrders
  );

  const selectedWorker = useSelector((state) => state.simulator.selectedWorker);

  const [time, setTime] = useState(Date.now());

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(Date.now());
      if (selectedWorker) dispatch(fetchWorkersOrdersAction(selectedWorker));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [time]);

  useEffect(() => {
    if (selectedWorker) dispatch(fetchWorkersOrdersAction(selectedWorker));
  }, [selectedWorker]);

  return (
    <>
      <div className="workerOrder">
        <Grid container spacing={3} className="workerContainer">
          <Grid item xs={12} className="workerContainerheader">
            <h1>Order List</h1>
          </Grid>
          {orderWorker.data ? (
            <>
              <div>
                <h1>Total Orders completed - {completedOrders}</h1>
              </div>
              {orderWorker.data.orders.map((data) => (
                <Grid item xs={12}>
                  <Paper className="orderContainer">
                    <div className="orderContainerHeader">
                      <div>
                        <h1>Order ID : {data.orders.id}</h1>
                      </div>
                      <div className="statusContainer">
                        <p>
                          {data.orders.isCompleted ? "COMPLETED" : "PROCESSING"}
                        </p>
                      </div>
                    </div>

                    <div className="cardContainer">
                      {data.orders.items.map((item) => (
                        <Card className="card">
                          <CardContent>
                            <h1>{item.name}</h1>
                            <p className="subHeader">{item.supplier}</p>
                            <div className="itemSubDetails">
                              <p>location - {item.location}</p>
                              <p>weight - {item.weight}</p>
                            </div>
                          </CardContent>
                        </Card>
                      ))}
                    </div>
                    <div className="orderContainerFooter">
                      <p>{convertDate(data.orders.createdAt)}</p>
                    </div>
                  </Paper>
                </Grid>
              ))}
            </>
          ) : (
            <div className="graphEmpty">
              <EmptyLoader width={100} height={100} />
              <h1>Please select the user to view their tasks</h1>
            </div>
          )}
        </Grid>
      </div>
    </>
  );
};

export default WorkerOrder;
