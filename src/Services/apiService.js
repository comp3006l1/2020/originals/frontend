import { getHeaders } from "../Utils/API";
import axios from "axios";

//storage
import { getFromStorage } from "../Utils/Storage";

/**
 *Api service
 *
 * @param {*} request payload
 * @returns response
 */
export const apiPostService = async (request, ENDPOINT) => {
  const API_ENDPOINT = ENDPOINT;

  let apiRes = null;
  let headers = getHeaders();

  try {
    apiRes = await axios.post(
      API_ENDPOINT,
      {
        ...request.payload,
      },
      {
        headers: headers,
      }
    );
  } catch (err) {
    //this.logErrors(err);
    apiRes = err.response;
  } finally {
    console.log(apiRes);
    return apiRes;
  }
};

/**
 *Api service
 *
 * @param {*} request payload
 * @returns response
 */
export const apiGetService = async (request, ENDPOINT) => {
  const API_ENDPOINT = ENDPOINT;

  let apiRes = null;
  let headers = getHeaders();

  try {
    apiRes = await axios.get(
      API_ENDPOINT,
      {
        ...request.payload,
      },
      {
        headers: headers,
      }
    );
  } catch (err) {
    //this.logErrors(err);
    apiRes = err.response;
  } finally {
    console.log(apiRes);
    return apiRes;
  }
};

/**
 *Api service
 *
 * @param {*} request payload
 * @returns response
 */
export const apiPutService = async (request, ENDPOINT) => {
  const API_ENDPOINT = ENDPOINT;

  let apiRes = null;
  let headers = getHeaders();

  try {
    apiRes = await axios.put(
      API_ENDPOINT,
      {
        ...request.payload,
      },
      {
        headers: headers,
      }
    );
  } catch (err) {
    //this.logErrors(err);
    apiRes = err.response;
  } finally {
    console.log(apiRes);
    return apiRes;
  }
};

/**
 *Api service
 *
 * @param {*} request payload
 * @returns response
 */
export const apiGetURLService = async (request, ENDPOINT) => {
  const API_ENDPOINT = ENDPOINT + request.payload;

  let apiRes = null;
  let headers = getHeaders();

  try {
    apiRes = await axios.get(
      API_ENDPOINT,
      // {
      //   ...request.payload,
      // },
      {
        headers: headers,
      }
    );
  } catch (err) {
    //this.logErrors(err);
    apiRes = err.response;
  } finally {
    console.log(apiRes);
    return apiRes;
  }
};
