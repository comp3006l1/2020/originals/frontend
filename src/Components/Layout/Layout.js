import React, {
  Fragment,
  Suspense,
  useContext,
  useState,
  useEffect,
} from "react";
import { Route, Redirect, useLocation, useHistory } from "react-router-dom";

import "./layout.scss";

import { MainLoader } from "../Loader";

const HomePage = React.lazy(() => import("../../Pages/Home"));

export const Layout = (props) => {
  const location = useLocation();
  const history = useHistory();

  return (
    <Fragment>
      <main className="main">
        <Suspense
          fallback={
            <div className="loaderContainer">
              <MainLoader height={300} width={300} />
            </div>
          }
        >
          <HomePage />
        </Suspense>
      </main>
    </Fragment>
  );
};

export default Layout;
