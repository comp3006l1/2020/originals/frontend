import React, { Fragment } from "react";
import Lottie from "react-lottie";

import "./loader.scss";

//assets
import { dsLoader, emptyOutput, emptyReport } from "../../Config/Lotties";

const lottieConfig = (file, loop = true) => {
  return {
    loop,
    autoplay: true,
    animationData: file,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
};

export const MainLoader = ({ height = 165, width = 151, infinite = true }) => {
  return (
    <div className="loaderComponent">
      <Lottie
        options={lottieConfig(dsLoader, infinite)}
        height={height}
        width={width}
        className="noReports"
      />
    </div>
  );
};

export const EmptyLoader = ({ height = 165, width = 151, infinite = true }) => {
  return (
    <div className="loaderComponent">
      <Lottie
        options={lottieConfig(emptyOutput, infinite)}
        height={height}
        width={width}
        className="noReports"
      />
    </div>
  );
};

export const EmptyReportLoader = ({
  height = 165,
  width = 151,
  infinite = true,
}) => {
  return (
    <div className="loaderComponent">
      <Lottie
        options={lottieConfig(emptyReport, infinite)}
        height={height}
        width={width}
        className="noReports"
      />
    </div>
  );
};
