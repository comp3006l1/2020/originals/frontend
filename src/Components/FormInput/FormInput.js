import React, { useState, useRef } from "react";

import {
  TextField,
  Select,
  MenuItem,
  Checkbox,
  Radio,
  FormControl,
  FormHelperText,
  FormLabel,
  InputLabel,
  Slider,
  Switch,
  FormControlLabel,
  RadioGroup,
} from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import { withStyles } from "@material-ui/core/styles";

//styles
import "./formInput.scss";

export const TextAreaInput = ({
  formClass = "",
  labelClass = "",
  labelValue = null,
  inputError = false,
  ...props
}) => {
  return (
    <FormControl
      className={`formInput ${formClass}`}
      error={inputError ? true : false}
    >
      {labelValue && (
        <FormLabel className={` ${labelClass}`}>{labelValue}</FormLabel>
      )}
      <TextField
        id="outlined-multiline-static"
        multiline
        variant="outlined"
        {...props}
      />
      <FormHelperText className="defaultHellperTxt">
        {inputError ? inputError : ""}
      </FormHelperText>
    </FormControl>
  );
};

/**
 *TextInput Component
 *
 * @param {*} { ...props }
 * @returns TextInput Component
 */
export const TextInput = ({
  formClass = "",
  labelClass = "",
  labelValue = null,
  inputError = false,
  inputRef = () => {},
  ...props
}) => {
  const testRef = useRef();

  return (
    <FormControl
      className={`formInput ${formClass}`}
      error={inputError ? true : false}
    >
      {labelValue && (
        <FormLabel className={` ${labelClass}`}>{labelValue}</FormLabel>
      )}
      <TextField
        id="outlined-basic"
        variant="outlined"
        error={inputError ? true : false}
        inputRef={inputRef}
        {...props}
      />
      <FormHelperText className="defaultHellperTxt">
        {inputError ? inputError : ""}
      </FormHelperText>
    </FormControl>
  );
};

/**
 *SelectBox Component
 *
 * @param {*} { data, className, ...props }
 * @returns SelectBox Component
 */
export const SelectBox = ({
  inputError = false,
  formClass = "",
  labelValue = "",
  labelClass = "",
  data,
  className,
  ...props
}) => {
  return (
    <FormControl
      className={`formInput ${formClass}`}
      error={inputError ? true : false}
    >
      {labelValue && (
        <FormLabel className={` ${labelClass}`}>{labelValue}</FormLabel>
      )}
      <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        variant="outlined"
        className={className}
        error={inputError ? true : false}
        // value={age}
        // onChange={handleChange}
        {...props}
      >
        {data.map((item) => (
          <MenuItem value={item.key}>{item.value}</MenuItem>
        ))}
      </Select>
      <FormHelperText className="defaultHellperTxt">
        {inputError ? inputError : ""}
      </FormHelperText>
    </FormControl>
  );
};

/**
 *Check box component
 *
 * @param {*} { className, ...props }
 * @returns ChechBox component
 */
export const CheckBox = ({ formLabel = "", className = "", ...props }) => {
  return (
    <FormControlLabel
      control={
        <Checkbox
          // checked={haveThisItem(data)}
          // onChange={() => handleSelectFile(data)}
          color="primary"
          className={`checkBox ${className}`}
          {...props}
        />
      }
      label={formLabel}
    />
  );
};

/**
 *RadioButton Component
 *
 * @param {*} { className, ...props }
 * @returns RadioButton Component
 */
export const RadioButton = ({ className, ...props }) => {
  return (
    <Radio
      // checked={selectedValue === "a"}
      // onChange={handleChange}
      // value="a"
      name="radio-button-demo"
      // inputProps={{ "aria-label": "A" }}
      className={`radioBox ${className}`}
      {...props}
    />
  );
};

/**
 *RadioButton Component
 *
 * @param {*} { className, ...props }
 * @returns RadioButton Component
 */
export const RadioButtonGroup = ({
  value = "",
  handleChange = () => {},
  className = "",
  groupClass = "",
  options = [],
  ...props
}) => {
  return (
    <RadioGroup
      aria-label="gender"
      name="gender1"
      value={value}
      onChange={handleChange}
      className={groupClass}
    >
      {options.map((data) => (
        <FormControlLabel
          value={data}
          control={<Radio className={`radioBox ${className}`} />}
          label={data}
        />
      ))}
    </RadioGroup>
  );
};

/**
 *Slider Component
 *
 * @param {*} { className, ...props }
 * @returns Slider Component
 */
export const ContinuousSlider = ({
  rangeMin = 0,
  rangeMax = 100,
  value = 20,
  success = false,
  className,
  ...props
}) => {
  return (
    <Slider
      value={value}
      defaultValue={30}
      className={`sliderDefault ${className}  ${success ? "success" : ""}`}
      min={rangeMin}
      max={rangeMax}
      {...props}
    />
  );
};

/**
 *DatePicker Component
 *
 * @param {*} { className, ...props }
 * @returns DatePicker Component
 */
export const DatePicker = ({
  value = "",
  handleDateChange = () => {},
  inputAdormantPosition = "end",
  className,
  ...props
}) => {
  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <KeyboardDatePicker
        autoOk
        variant="inline"
        inputVariant="outlined"
        format="MM/DD/YYYY"
        inputValue=""
        value={value}
        InputAdornmentProps={{ position: inputAdormantPosition }}
        onChange={handleDateChange}
        placeholder="Select a date to view the uploaded files"
        {...props}
      />
    </MuiPickersUtilsProvider>
  );
};
