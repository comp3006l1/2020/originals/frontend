import React, { useState, useEffect } from "react";

import Modal from "@material-ui/core/Modal";

//assets
import { closeBtn } from "../../Config/Images";

//components
import { TextInput } from "../FormInput";
import { PrimaryButton } from "../Buttons";

import "./modal.scss";

export const ModalComponent = ({
  children,
  open = false,
  handleClose = () => {},
  showCloseBtn = false,
  ...props
}) => {
  return (
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={open}
      onClose={handleClose}
      {...props}
      className={`modalComponent`}
    >
      <div className="modalBody">
        {showCloseBtn ? (
          <img
            src={closeBtn}
            className="closeBtn"
            alt="close"
            onClick={handleClose}
          />
        ) : (
          ""
        )}
        {children}
      </div>
    </Modal>
  );
};
