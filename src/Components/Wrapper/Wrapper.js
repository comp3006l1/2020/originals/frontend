import React, { useState, useRef, useEffect } from "react";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

//assets
import { fileLogo } from "../../Config/Images";
import Pagination from "../Pagination/Pagination";

import "./wrapper.scss";

//report util
import ReportUtil from "../../Pages/VerticalTopo/Utils/ReportUtils";

export const ResultPane = ({ className, children, ...props }) => {
  return (
    <div className={`resultsPane ${className}`} {...props}>
      {children}
    </div>
  );
};

export const TableView = ({ tableClass, data, ...props }) => {
  return (
    <TableContainer>
      <Table className={tableClass} aria-label="simple table">
        <TableHead>
          <TableRow>
            {data.header.map((row) => (
              <TableCell>{row.value}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.data.map((row) => (
            <TableRow key={row.name}>
              {data.header.map((val) => {
                return val.key === "file" ? (
                  <TableCell component="th" scope="row" className="fileDataRow">
                    <div className="d-flex">
                      <img src={fileLogo} alt="file" />
                      <span className="tb-data">{row[val.key]}</span>
                    </div>
                  </TableCell>
                ) : (
                  <TableCell component="th" scope="row">
                    {row[val.key]}
                  </TableCell>
                );
              })}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export const ReportsTableView = ({
  tableClass,
  onClickRow = () => {},
  changePage = () => {},
  totalCount = 0,
  data,
  ...props
}) => {
  return (
    <TableContainer>
      <Table className={tableClass} aria-label="simple table">
        <TableBody>
          {data.data.map((row, index) => (
            <TableRow key={index} onClick={() => onClickRow(row.id)}>
              {data.header.map((val) => {
                return val.key === "file" ? (
                  <TableCell component="th" scope="row" className="fileDataRow">
                    <img src={fileLogo} alt="file" />
                    <span className="tb-data">{row[val.key]}</span>
                  </TableCell>
                ) : val.key === "date" ? (
                  <TableCell component="th" scope="row" className="dateDataRow">
                    {row[val.key]}
                  </TableCell>
                ) : (
                  <TableCell component="th" scope="row">
                    {row[val.key]}
                  </TableCell>
                );
              })}
            </TableRow>
          ))}
        </TableBody>
      </Table>
      {data.data.length !== 0 && (
        <Pagination totalRecords={totalCount} changePage={changePage} />
      )}
    </TableContainer>
  );
};
