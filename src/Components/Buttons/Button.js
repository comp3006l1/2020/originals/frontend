import React, { useState } from "react";

import { Button } from "@material-ui/core";

import "./button.scss";

export const OutlineButton = ({ className, children, ...props }) => {
  return (
    <Button className={`outline-btn ${className}`} {...props}>
      {children}
    </Button>
  );
};

export const PrimaryButton = ({
  className,
  loading = false,
  children,
  ...props
}) => {
  return (
    <Button
      className={`primary-btn ${className}`}
      disabled={loading}
      {...props}
    >
      {children}
    </Button>
  );
};

export const DefaultButton = ({ className, children, ...props }) => {
  return (
    <Button className={`default-btn ${className}`} {...props}>
      {children}
    </Button>
  );
};

export const SecondaryOutlineButton = ({ className, children, ...props }) => {
  return (
    <Button className={`secondary-outline-btn ${className}`} {...props}>
      {children}
    </Button>
  );
};
