import axios from "axios";
// import { UNAUTH_USER } from "../actions/types";

export default {
  setupInterceptors: (store) => {
    // Add a response interceptor
    axios.interceptors.response.use(
      async (response) => {
        console.log("axios response");
        return response;
      },
      async (error) => {
        console.log("axios working");

        const originalRequest = error.config;
        // const serverCallUrl = new URL(originalRequest.url);
        const status = error.response.status;

        if (
          (status === 401 || status === 403) &&
          !originalRequest._retry
          // &&
          // !serverCallUrl.pathname.includes("/auth")
        ) {
          console.log("axios catched");
          // let token = await refreshAccessToken();
          // setAccessToken(token);

          // originalRequest._retry = true;
          // originalRequest.headers.authorization = `Bearer ${token}`;
          // signOut();

          return axios(originalRequest);
        }

        return Promise.reject(error);
      }
    );
  },
};
