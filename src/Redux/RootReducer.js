import { combineReducers } from "redux";

import * as types from "../Store/ActionTypes";

//reducers
import clock from "./Clock/ClockReducer";
import order from "./Order/OrderReducer";
import simulator from "./Simulator/SimulatorReducer";
import managment from "./Managment/ManagmentReducer";

const rootReducer = combineReducers({ clock, order, simulator, managment });

export default rootReducer;
