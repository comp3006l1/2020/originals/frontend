import * as types from "../../Store/ActionTypes";

export const fetchWorkersAction = (response) => {
  return {
    type: types.FETCH_WORKERS,
    response,
  };
};

export const selectWorkersAction = (response) => {
  return {
    type: types.SELECT_WORKERS,
    response,
  };
};

export const setConfigurationAction = (response) => {
  return {
    type: types.SET_CONFIGURATION,
    response,
  };
};

export const submitConfigurationAction = (payload) => {
  return {
    type: types.SUBMIT_CONFIGURATION,
    payload,
  };
};

export const getConfigurationAction = (payload) => {
  return {
    type: types.GET_CONFIGURATION,
    payload,
  };
};

export const getWorkerByIDAction = (payload) => {
  return {
    type: types.GET_WORKER_BY_ID,
    payload,
  };
};

export const getMapAction = (payload) => {
  return {
    type: types.GET_MAP,
    payload,
  };
};
