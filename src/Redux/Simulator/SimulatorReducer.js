import * as types from "../../Store/ActionTypes";

const initailState = {
  workers: {
    status: null,
    data: null,
  },
  configuration: {},
  config: {
    status: null,
    data: null,
  },
  selectedWorker: null,
  selectedWorkerDetail: {
    status: null,
    data: null,
  },
  map: {
    status: null,
    data: null,
  },
};

export default function (state = initailState, action) {
  let response = action.response;

  switch (action.type) {
    case types.FETCH_WORKERS_SUCCESS:
      return {
        ...state,
        workers: {
          ...state.workers,
          status: response.status,
          data: response.data,
        },
      };
    case types.FETCH_WORKERS_ERROR:
      return {
        ...state,
        workers: response
          ? response.status && response.data
            ? {
                ...state.workers,
                status: response.status,
                data: response.data,
              }
            : {
                status: 400,
                data: null,
              }
          : {
              status: 400,
              data: null,
            },
      };
    case types.SUBMIT_CONFIGURATION_SUCCESS:
      return {
        ...state,
        config: {
          ...state.config,
          status: response.status,
          data: response.data,
        },
        selectedWorker: null,
      };
    case types.SUBMIT_CONFIGURATION_ERROR:
      return {
        ...state,
        config: response
          ? response.status && response.data
            ? {
                ...state.config,
                status: response.status,
                data: response.data,
              }
            : {
                status: 400,
                data: null,
              }
          : {
              status: 400,
              data: null,
            },
      };
    case types.GET_MAP_SUCCESS:
      return {
        ...state,
        map: {
          ...state.map,
          status: response.status,
          data: response.data,
        },
      };
    case types.GET_MAP_ERROR:
      return {
        ...state,
        map: response
          ? response.status && response.data
            ? {
                ...state.map,
                status: response.status,
                data: response.data,
              }
            : {
                status: 400,
                data: null,
              }
          : {
              status: 400,
              data: null,
            },
      };
    case types.GET_WORKER_BY_ID_SUCCESS:
      return {
        ...state,
        selectedWorkerDetail: {
          ...state.selectedWorkerDetail,
          status: response.status,
          data: response.data,
        },
      };
    case types.GET_WORKER_BY_ID_ERROR:
      return {
        ...state,
        selectedWorkerDetail: response
          ? response.status && response.data
            ? {
                ...state.selectedWorkerDetail,
                status: response.status,
                data: response.data,
              }
            : {
                status: 400,
                data: null,
              }
          : {
              status: 400,
              data: null,
            },
      };
    case types.SET_CONFIGURATION:
      return {
        ...state,
        configuration: { ...state.configuration, ...response },
      };

    case types.SELECT_WORKERS:
      return {
        ...state,
        selectedWorker: response,
      };

    default:
      return state;
  }
}
