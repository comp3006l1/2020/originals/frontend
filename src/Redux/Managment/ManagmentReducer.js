import * as types from "../../Store/ActionTypes";

const initailState = {
  orderWorker: {
    status: null,
    data: null,
  },
  completedOrders: 0,
};

export default function (state = initailState, action) {
  let response = action.response;

  switch (action.type) {
    case types.FETCH_WORKER_ORDERS_SUCCESS:
      return {
        ...state,
        orderWorker: {
          ...state.orderWorker,
          status: response.status,
          data: response.data,
        },
        completedOrders: response.data
          ? response.data.orders.filter((x) => x.orders.isCompleted).length
          : 0,
      };
    case types.FETCH_WORKER_ORDERS_ERROR:
      return {
        ...state,
        orderWorker: response
          ? response.status && response.data
            ? {
                ...state.orderWorker,
                status: response.status,
                data: response.data,
              }
            : {
                status: 400,
                data: null,
              }
          : {
              status: 400,
              data: null,
            },
      };

    default:
      return state;
  }
}
