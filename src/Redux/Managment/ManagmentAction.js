import * as types from "../../Store/ActionTypes";

export const fetchWorkersOrdersAction = (payload) => {
  return {
    type: types.FETCH_WORKER_ORDERS,
    payload,
  };
};
