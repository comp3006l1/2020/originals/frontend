import * as types from "../../Store/ActionTypes";

export const fetchOrderAction = (response) => {
  return {
    type: types.FETCH_ORDERS,
    response,
  };
};
