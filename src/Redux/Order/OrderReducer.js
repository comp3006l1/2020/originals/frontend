import * as types from "../../Store/ActionTypes";

const initailState = {
  order: {
    status: null,
    data: null,
  },
};

export default function (state = initailState, action) {
  let response = action.response;

  switch (action.type) {
    case types.FETCH_ORDERS_SUCCESS:
      return {
        ...state,
        order: {
          ...state.order,
          status: response.status,
          data: response.data,
        },
      };
    case types.FETCH_ORDERS_ERROR:
      return {
        ...state,
        order: response
          ? response.status && response.data
            ? {
                ...state.order,
                status: response.status,
                data: response.data,
              }
            : {
                status: 400,
                data: null,
              }
          : {
              status: 400,
              data: null,
            },
      };

    default:
      return state;
  }
}
