import * as types from "../../Store/ActionTypes";

export const fetchClockAction = (response) => {
  return {
    type: types.FETCH_CLOCK,
    response,
  };
};
