import * as types from "../../Store/ActionTypes";

const initailState = {
  clock: {
    status: null,
    data: null,
  },
};

export default function (state = initailState, action) {
  let response = action.response;

  switch (action.type) {
    case types.FETCH_CLOCK_SUCCESS:
      return {
        ...state,
        clock: {
          ...state.clock,
          status: response.status,
          data: response.data,
        },
      };
    case types.FETCH_CLOCK_ERROR:
      return {
        ...state,
        clock: response
          ? response.status && response.data
            ? {
                ...state.clock,
                status: response.status,
                data: response.data,
              }
            : {
                status: 400,
                data: null,
              }
          : {
              status: 400,
              data: null,
            },
      };

    default:
      return state;
  }
}
