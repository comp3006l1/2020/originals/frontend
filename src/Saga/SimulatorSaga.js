import { put, call, select } from "redux-saga/effects";

//API calls
import {
  apiGetService,
  apiPutService,
  apiGetURLService,
} from "../Services/apiService";

//redux actions
import * as types from "../Store/ActionTypes";

//api endpoints
import { GET_WORKERS, PUT_CONFIG, GET_MAP } from "../Utils/APIEndpoint";

/**
 *Workers
 *
 * @export
 * @param {*} payload payload object
 */
export function* fetchWorkersSaga(payload) {
  let response = null;
  try {
    response = yield call(apiGetService, payload, GET_WORKERS);

    if (response.data.success && response.status == 200) {
      yield put({ type: types.FETCH_WORKERS_SUCCESS, response });
    } else {
      yield put({ type: types.FETCH_WORKERS_ERROR, response });
    }
  } catch (error) {
    yield put({ type: types.FETCH_WORKERS_ERROR, response });
  }
}

/**
 *config
 *
 * @export
 * @param {*} payload payload object
 */
export function* setConfigSaga(payload) {
  let response = null;
  try {
    response = yield call(apiPutService, payload, PUT_CONFIG);

    if (response.data.success && response.status == 200) {
      yield put({ type: types.SUBMIT_CONFIGURATION_SUCCESS, response });
    } else {
      yield put({ type: types.SUBMIT_CONFIGURATION_ERROR, response });
    }
  } catch (error) {
    yield put({ type: types.SUBMIT_CONFIGURATION_ERROR, response });
  }
}

/**
 *config
 *
 * @export
 * @param {*} payload payload object
 */
export function* getWorkerByID(payload) {
  let response = null;
  try {
    response = yield call(apiGetURLService, payload, GET_WORKERS + "/");

    if (response.data.success && response.status == 200) {
      yield put({ type: types.GET_WORKER_BY_ID_SUCCESS, response });
    } else {
      yield put({ type: types.GET_WORKER_BY_ID_ERROR, response });
    }
  } catch (error) {
    yield put({ type: types.GET_WORKER_BY_ID_ERROR, response });
  }
}

/**
 *map
 *
 * @export
 * @param {*} payload payload object
 */
export function* getMap(payload) {
  let response = null;
  try {
    response = yield call(apiGetService, payload, GET_MAP);

    if (response.data.success && response.status == 200) {
      yield put({ type: types.GET_MAP_SUCCESS, response });
    } else {
      yield put({ type: types.GET_MAP_ERROR, response });
    }
  } catch (error) {
    yield put({ type: types.GET_MAP_ERROR, response });
  }
}
