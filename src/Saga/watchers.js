import { takeLatest, takeEvery } from "redux-saga/effects";

//action types
import * as types from "../Store/ActionTypes";

//saga
import { fetchClockSaga } from "./ClockSaga";
import { fetchOrdersSaga } from "./OrderSaga";
import {
  fetchWorkersSaga,
  setConfigSaga,
  getWorkerByID,
  getMap,
} from "./SimulatorSaga";
import { fetchWorkersOrdersSaga } from "./ManagmentSaga";

export default function* watchUserAuthentication() {
  yield takeEvery(types.FETCH_CLOCK, fetchClockSaga);
  yield takeEvery(types.FETCH_ORDERS, fetchOrdersSaga);
  yield takeEvery(types.FETCH_WORKERS, fetchWorkersSaga);
  yield takeEvery(types.SUBMIT_CONFIGURATION, setConfigSaga);
  yield takeEvery(types.GET_WORKER_BY_ID, getWorkerByID);
  yield takeEvery(types.GET_MAP, getMap);
  yield takeEvery(types.FETCH_WORKER_ORDERS, fetchWorkersOrdersSaga);
}
