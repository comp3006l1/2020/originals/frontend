import { put, call, select } from "redux-saga/effects";

//API calls
import { apiGetService } from "../Services/apiService";

//redux actions
import * as types from "../Store/ActionTypes";

//api endpoints
import { GET_ORDERS } from "../Utils/APIEndpoint";

/**
 *orders
 *
 * @export
 * @param {*} payload payload object
 */
export function* fetchOrdersSaga(payload) {
  let response = null;
  try {
    response = yield call(apiGetService, payload, GET_ORDERS);

    console.log(
      "respo",
      response,
      response.data.success,
      response.status == 201
    );

    if (response.data.success && response.status == 200) {
      yield put({ type: types.FETCH_ORDERS_SUCCESS, response });
    } else {
      yield put({ type: types.FETCH_ORDERS_ERROR, response });
    }
  } catch (error) {
    yield put({ type: types.FETCH_ORDERS_ERROR, response });
  }
}
