import { put, call, select } from "redux-saga/effects";

//API calls
import { apiGetService } from "../Services/apiService";

//redux actions
import * as types from "../Store/ActionTypes";

//api endpoints
import { GET_CLOCK_STEP } from "../Utils/APIEndpoint";

/**
 *Create file Saga
 *
 * @export
 * @param {*} payload payload object
 */
export function* fetchClockSaga(payload) {
  let response = null;
  try {
    response = yield call(apiGetService, payload, GET_CLOCK_STEP);

    console.log(
      "respo",
      response,
      response.data.success,
      response.status == 201
    );

    if (response.data.success && response.status == 200) {
      yield put({ type: types.FETCH_CLOCK_SUCCESS, response });
    } else {
      yield put({ type: types.FETCH_CLOCK_ERROR, response });
    }
  } catch (error) {
    yield put({ type: types.FETCH_CLOCK_ERROR, response });
  }
}
