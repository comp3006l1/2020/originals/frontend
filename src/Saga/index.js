import { fork } from "redux-saga/effects";
import watcherFunction from "./watchers";

export default function* startForman() {
  yield fork(watcherFunction);
}
