import { put, call, select } from "redux-saga/effects";

//API calls
import {
  apiGetService,
  apiPutService,
  apiGetURLService,
} from "../Services/apiService";

//redux actions
import * as types from "../Store/ActionTypes";

//api endpoints
import { GET_WORKER_ORDERS } from "../Utils/APIEndpoint";

/**
 *Workers
 *
 * @export
 * @param {*} payload payload object
 */
export function* fetchWorkersOrdersSaga(payload) {
  let response = null;
  try {
    response = yield call(apiGetURLService, payload, GET_WORKER_ORDERS + "/");

    if (response.data.success && response.status == 201) {
      yield put({ type: types.FETCH_WORKER_ORDERS_SUCCESS, response });
    } else {
      yield put({ type: types.FETCH_WORKER_ORDERS_ERROR, response });
    }
  } catch (error) {
    yield put({ type: types.FETCH_WORKER_ORDERS_ERROR, response });
  }
}
