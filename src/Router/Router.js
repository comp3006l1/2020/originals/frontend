import React, { Fragment, useContext } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

//import routers
import Layout from "../Components/Layout";

/**
 *Defining Main Routes
 *
 * @returns
 */
const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Fragment>
          {/* <Route
            exact
            path="/app"
            render={() => <Redirect to="/app/upload" />}
          /> */}
          <Route exact path="/" component={Layout}></Route>
        </Fragment>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
