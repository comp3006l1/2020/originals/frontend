import axios from "axios";
import { getFromStorage } from "../Utils/Storage";

//constants
import { REACT_APP_API_SERVER } from "../Config/Constants";

export const getHeaders = () => {
  let token = getFromStorage("token");
  let headers = {
    Accept: "application/json",
  };
  if (typeof token !== "undefined" && token) {
    headers.Authorization = " Bearer " + token;
    console.log("headers.Authorization ", headers.Authorization);
  }

  return headers;
};

export const axiosIntstance = axios.create({
  baseURL: REACT_APP_API_SERVER,
  responseType: "json",
  // headers: getHeaders(),
});
