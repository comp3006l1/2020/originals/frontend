// Define validationStateSchema
export const validationStateSchema = {
  displayName: {
    required: true,
    validator: {
      regEx: /^.{1,100}$/,
      emptyError: "Please input your name.",
      error:
        "Name must be less than 100 letters and should not contain any special characters.",
    },
  },
};

/**
 * Validate Inputs
 * @param {String} inputName state value in a state key
 * @param {String} validatorKey key value in the validationStateSchema
 * */
export const validateSingleInput = (inputName, validatorKey) => {
  // validations helper function

  if (
    validationStateSchema[validatorKey].validator !== null &&
    typeof validationStateSchema[validatorKey].validator === "object"
  ) {
    //check whether validation schema required item
    if (
      !inputName ||
      !validationStateSchema[validatorKey].validator.regEx.test(inputName)
    ) {
      console.log(" regrex failed");
      let thisError;
      thisError = !validationStateSchema[validatorKey].validator.regEx.test(
        inputName
      )
        ? validationStateSchema[validatorKey].validator.error
        : thisError;
      thisError = !inputName
        ? validationStateSchema[validatorKey].validator.emptyError
        : thisError;

      // console.log(thisError);

      return thisError;
    }
  }
  return false;
};
