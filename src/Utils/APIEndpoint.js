export const GET_CLOCK_STEP = "http://localhost:9000/clock";
export const GET_ORDERS = "https://ordergen.herokuapp.com/orders";
export const GET_WORKERS = "http://localhost:8300/workers";
export const PUT_CONFIG = "http://localhost:8300/configuration";
export const GET_MAP = "http://localhost:8300/map";
export const GET_WORKER_ORDERS =
  "https://managmentservice.herokuapp.com/orders";
