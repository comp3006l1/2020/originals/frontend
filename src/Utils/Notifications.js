import React from "react";
import { toast } from "react-toastify";

export const successNoty = ({ msg = "", timeout = 5000 }) => {
  return toast.success(msg, {
    position: "top-center",
    autoClose: timeout,
    hideProgressBar: false,
    closeOnClick: false,
    pauseOnHover: true,
    draggable: true,
  });
};

export const errorNoty = ({ msg = "", timeout = 5000 }) => {
  return toast.error(msg, {
    position: "top-center",
    autoClose: timeout,
    hideProgressBar: false,
    closeOnClick: false,
    pauseOnHover: true,
    draggable: true,
  });
};
