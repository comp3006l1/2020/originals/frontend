/**
 *Get from localstorage
 *
 * @param {*} name name of item
 * @returns object stored in localstorage
 */
export const getFromStorage = (name) => {
  let data = localStorage.getItem(name);

  if (!data || typeof data === "undefined") {
    return "";
  }

  return data;
};

/**
 *Store user in localstorage
 *
 * @param {*} user user object
 */
export const setUser = (user) => {
  localStorage.setItem("user", JSON.stringify(user));
};

/**
 * Clear everything in local storage
 *
 *
 */
export const clearStorage = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("user");
  // localStorage.removeItem("project");
};
